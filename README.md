# Project Title

One Paragraph of project description goes here

## Summary

  - [Environment](#environment)
  - [Installing](#installing)
  - [Deployment](#deployment)
  - [Versioning](#versioning)
  - [Known bugs (optional)](#known-bugs)
  - [Other (optional)](#other)

## Environment
- npm ^5.6.0
- node ~8.10.0
- php 7.34
- OS linux

## Installing
A step by step series of examples that tell you have to get a development env running

For install packages run:

```npm install ```
    
    
To start the project:

```gulp watch --dev ```

## Deployment
A step by step series of examples that tell you have to deploy the project

```gulp build --prod```
 
The build will be in the folder ```dist```

## Versioning
- MAJOR version when you make incompatible API changes,
- MINOR version when you add functionality in a backwards compatible manner
- PATCH version when you make backwards compatible bug fixes.

For more details: [SemVer](https://semver.org/lang/uk/)

## Known bugs
Possible bugs and their fixes

## Other 
Some features of the project, something that does not fit the previous paragraphs.
How to connect fonts, how to connect third-party libraries, explanations on commands, etc.

